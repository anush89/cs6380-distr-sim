package cs6380simulator.nodes;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import cs6380simulator.algorithms.Algorithm;

public class AsyncNode extends Node {

	public AsyncNode(int id, List<Algorithm> algorithms, CyclicBarrier sentinel) {
		super(id, algorithms, sentinel);
	}

	@Override
	protected void await() throws InterruptedException, BrokenBarrierException {
		if (this.finalResult != null){
			sentinel.await();
		}
		
	}

}
