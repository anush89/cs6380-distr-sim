package cs6380simulator.nodes;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import cs6380simulator.algorithms.Algorithm;


public class SyncNode extends Node {
	
	public SyncNode(int id, List<Algorithm> algorithms, CyclicBarrier sentinel) {
		super(id, algorithms, sentinel);
	}

	@Override
	protected void await() throws InterruptedException, BrokenBarrierException {
		sentinel.await();
	}
	
	
	
}
