package cs6380simulator;
import java.util.Queue;

/**
 * Represents a unidirectional link between two nodes
 */
public class Link {
	
	private int id;
	public double Weight;
	public Queue<String> Messages;
	
	/**
	 * Instantiates a new Link instance
	 * @param id The Id for the node at the other end of the link
	 * @param weight The weight for the link
	 * @param messageQueue The message queue for the link
	 */
	public Link(int id, double weight, Queue<String> messageQueue){
		this.id = id;
		this.Weight = weight;
		this.Messages = messageQueue;
	}
	
	/**
	 * Returns the node ID of the other end of the link
	 * @return The integer node ID of the other end of the link
	 */
	public int GetId(){
		return id;
	}

	
}
