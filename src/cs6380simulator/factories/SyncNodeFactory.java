package cs6380simulator.factories;

import java.util.concurrent.CyclicBarrier;

import cs6380simulator.nodes.SyncNode;

public class SyncNodeFactory extends NodeFactory<SyncNode>{

	public SyncNodeFactory(AlgorithmSet algorithmSet, CyclicBarrier sentinel) {
		super(algorithmSet, sentinel);
		// TODO Auto-generated constructor stub
	}

	@Override
	public SyncNode Create(int nid) {
		return new SyncNode(nid, getAlgorithmSet(nid), sentinel);
	}
}
