package cs6380simulator.factories;

import java.util.concurrent.CyclicBarrier;

import cs6380simulator.nodes.AsyncNode;

public class AsyncNodeFactory extends NodeFactory<AsyncNode> {

	public AsyncNodeFactory(AlgorithmSet algorithmSet, CyclicBarrier sentinel) {
		super(algorithmSet, sentinel);
	}

	@Override
	public AsyncNode Create(int nid) {
		return new AsyncNode(nid, getAlgorithmSet(nid), sentinel);
	}

}
