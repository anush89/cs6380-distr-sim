package cs6380simulator.factories;

import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;

import cs6380simulator.algorithms.Algorithm;
import cs6380simulator.algorithms.BFS;
import cs6380simulator.algorithms.FloodMax;
import cs6380simulator.algorithms.FloodMaxAcks;
import cs6380simulator.nodes.Node;


public abstract class NodeFactory<T extends Node> {
	
	private AlgorithmSet algorithmSet;
	protected CyclicBarrier sentinel;
	
	public NodeFactory(AlgorithmSet algorithmSet, CyclicBarrier sentinel){
		this.algorithmSet = algorithmSet;
		this.sentinel = sentinel;
	}
	
	protected ArrayList<Algorithm> getAlgorithmSet(int nid){
		ArrayList<Algorithm> algorithms = new ArrayList<Algorithm>();
		
		switch (algorithmSet){
			case FloodMax:
				algorithms.add(new BFS(nid));
				algorithms.add(new FloodMax(nid));
				break;
			case FloodMaxWithAcks:
				algorithms.add(new FloodMaxAcks(nid));
				break;
			default:
				throw new IllegalArgumentException("The specified algorithm set has not been implemented or is invalid.");
		}
		
		return algorithms;
	}
	
	// Instantiate an instance of the node
	public abstract T Create(int nid);
	
	/**
	 * A list of valid algorithm sets
	 *
	 */
	public enum AlgorithmSet {
		FloodMax, FloodMaxWithAcks
	}
}
